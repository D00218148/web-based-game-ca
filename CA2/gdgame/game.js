/********************************************************************* EVENT LISTENERS *********************************************************************/
//add event listener for load
window.addEventListener("load", LoadGame);

/********************************************************************* GLOBAL VARS *********************************************************************/
//get a handle to our canvas
var cvs = document.getElementById("game-canvas");
//get a handle to 3D context which allows drawing
var ctx = cvs.getContext("2d");

//stores elapsed time
var gameTime;
var health = 3;

//fps variables
var times = [];
var fps;

//assets
var spriteSheet, backgroundSpriteSheet;

//managers and notification
var objectManager;
var soundManager;
var keyboardManager;
var collisionManager;
var notificationCenter;

//stores screen bounds in a rectangle
var screenRectangle;
var boundaryRectangle;

/************************************************************ CORE GAME LOOP CODE UNDER THIS POINT ************************************************************/

// #region  LoadGame, Start, Animate
function LoadGame() {
  
  //load content
  Initialize();

  //start timer
  Start();
 
  //publish an event to pause the object manager (i.e. no update, no draw) and show the menu
 this.notificationCenter.Notify(new Notification(NotificationType.Menu, NotificationAction.ShowMenuChanged,  [StatusType.Off]));
}

function Start() {
  //runs in proportion to refresh rate
  this.animationTimer = window.requestAnimationFrame(Animate);
  this.gameTime = new GameTime();
  
  RefreshLoop();
}

function Animate(now) {
  this.gameTime.Update(now);
  Update(this.gameTime);
  Draw(this.gameTime);
  window.requestAnimationFrame(Animate);
}

// #endregion

// #region  Update, Draw
function Update(gameTime) {
  //update all the game sprites
  this.objectManager.Update(gameTime);
  this.collisionManager.Update(gameTime);

  //updates the menu manager to listen for show/hide menu keystroke
  this.menuManager.Update(gameTime); 
}

//Code for calculating and displaying fps can be found
//on stack overflow:
//https://stackoverflow.com/questions/8279729/calculate-fps-in-canvas-using-requestanimationframe
function RefreshLoop() { //calculates fps
  window.requestAnimationFrame(function() {
    const now = performance.now();
    while (times.length > 0 && times[0] <= now - 1000) {
      times.shift();
    }
    times.push(now);
    fps = times.length;
    ShowFPS();
    RefreshLoop();
  });
}

function ShowFPS(){ //displays fps
  ctx.fillStyle = "Orange";
  ctx.font      = "normal 16pt Arial";

  ctx.fillText(fps + " fps", 10, 26);
}

function Draw(gameTime) {
  //clear screen on each draw of ALL sprites (i.e. menu and game sprites)
  ClearScreen(Color.Black);

  //draw all the game sprites
  this.objectManager.Draw(gameTime);

  ctx.fillStyle = "Orange";
  ctx.font      = "normal 16pt Arial";

  ctx.fillText("score: " + this.objectManager.score, 10, 46);

  ctx.fillStyle = "Orange";
  ctx.font      = "normal 16pt Arial";
  ctx.fillText("lives: " + this.objectManager.health, 10, 66);

}

function ClearScreen(color) {
  ctx.save();
  ctx.fillStyle = color;
  ctx.fillRect(0, 0, cvs.clientWidth, cvs.clientHeight);
  ctx.restore();
}

// #endregion

// #region  Initialize, Load
function Initialize() {
  InitializeScreenRectangle();
  //InitializeBoundaryRectangle();
  LoadAssets();
  LoadNotificationCenter();
  LoadManagers();
  LoadSprites();
  this.isPlaying = false;
}

function InitializeScreenRectangle()
{
  this.screenRectangle = new Rect(cvs.clientLeft, cvs.clientTop, cvs.clientWidth, cvs.clientHeight);
}

/*function InitializeBoundaryRectangle()
{
  this.boundaryRectangle = new Rect(cvs.clientLeft-500, cvs.clientTop, cvs.clientWidth+500, cvs.clientHeight);
}*/

function LoadNotificationCenter()
{
  this.notificationCenter = new NotificationCenter();
}

function LoadManagers() {

  //renders game sprites
  let debugEnabled = false;
  this.objectManager = new CAMyObjectManager("game sprites", this.ctx, debugEnabled, this.notificationCenter);

  //checks for keyboard input
  this.keyboardManager = new KeyboardManager();

  //plays sound
  this.soundManager = new SoundManager(audioCueArray);

  //Collision
  this.collisionManager = new CollisionManager(objectManager, soundManager, screenRectangle);

  //adds support for a menu system
  this.menuManager = new MyMenuManager(this.notificationCenter, this.keyboardManager);
}

function LoadAssets() {
  //textures
  this.backgroundSpriteSheet = document.getElementById("background");
  this.spriteSheet = document.getElementById("shaggy_spritesheet");
  this.pickupSpriteSheet = document.getElementById("food_spritesheet");
  this.stripeSpriteSheet = document.getElementById("stripe_spritesheet");
  this.michaelSpriteSheet = document.getElementById("michael-jackson_spritesheet");
  this.jasonSpriteSheet = document.getElementById("jason_spritesheet");
  this.freddySpriteSheet = document.getElementById("freddy_spritesheet");
  this.terminatorSpriteSheet = document.getElementById("terminator_spritesheet");
  this.skaterSpriteSheet = document.getElementById("gremlin-Skater_spritesheet");
  this.garfieldSpriteSheet = document.getElementById("garfield_spritesheet");
  this.creeperSpriteSheet = document.getElementById("creeper_spritesheet");
  this.garfieldSpriteSheet = document.getElementById("garfield_spritesheet");
  this.creeperSpriteSheet = document.getElementById("creeper_spritesheet");
  this.jokerSpriteSheet = document.getElementById("joker_spritesheet");
  this.scorpionSpriteSheet = document.getElementById("scorpion_spritesheet");
}

function LoadSprites() {

  LoadBackground();

  //load game sprites here...
  window.setInterval(LoadPickups, 1500); // continuously calls function every 1.5 secs
  LoadPickups();
  window.setInterval(LoadEnemies, 3000); // continuously calls function every 3 secs
  LoadPlayer();
}

function LoadBackground() {
  //Background
  var spriteArtist = new SpriteArtist(ctx, backgroundSpriteSheet,
    new Vector2(0,0), new Vector2(800, 600));

  var transform = new Transform2D(
    new Vector2(0, 0),
    0,
    new Vector2(1, 1),
    new Vector2(0, 0),
    new Vector2(cvs.clientWidth, cvs.clientHeight)
  );
  this.objectManager.Add(
    new Sprite(
      "background",
      ActorType.Background,
      transform,
      spriteArtist,
      StatusType.IsUpdated | StatusType.IsDrawn
    )
  );
}

function LoadEnemies() {
  var enemy = Math.floor(Math.random() * 10) + 1;

  switch(enemy) {  //Chooses a random enemy to spawn

      case 1:
        var direction = Math.floor(Math.random() * 2) + 1;  //generate random direction

        switch(direction){
          case 1:
            var chosenDirection = ENEMY_MOVE_DIRECTION;
            var startXPosition = ENEMY_START_X_POSITION;
            var cells = STRIPE_CELLS_RIGHT;
            break;

          case 2:
            var chosenDirection = ENEMY_MOVE_DIRECTION2;
            var startXPosition = ENEMY_START_X_POSITION2;
            var cells = STRIPE_CELLS_LEFT;
            break;
        }

        var spriteArtist = new AnimatedSpriteArtist(
          ctx,
          this.stripeSpriteSheet,
          RUNNER_ANIMATION_FPS,
          cells, //used to access animation sprites for either right walk or left walk - see MyConstants
          0
        );

        var transform = new Transform2D(
          new Vector2(startXPosition, STRIPE_START_Y_POSITION),
          0,
          new Vector2(1, 1),
          new Vector2(0, 0),
          new Vector2(STRIPE_CELLS_WIDTH, STRIPE_CELLS_HEIGHT)  //used for CD/CR rectangle - see MyConstants
        );

        var enemySpeed = STRIPE_MOVE_SPEED;  //set the speed of the selected enemy
        break;

      case 2:
        var direction = Math.floor(Math.random() * 2) + 1;  

        switch(direction){
          case 1:
            var chosenDirection = ENEMY_MOVE_DIRECTION;
            var startXPosition = ENEMY_START_X_POSITION;
            var cells = MICHAEL_CELLS_RIGHT;
            break;

          case 2:
            var chosenDirection = ENEMY_MOVE_DIRECTION2;
            var startXPosition = ENEMY_START_X_POSITION2;
            var cells = MICHAEL_CELLS_LEFT;
            break;
        }

        var spriteArtist = new AnimatedSpriteArtist(
          ctx,
          this.michaelSpriteSheet,
          MICHAEL_ANIMATION_FPS,
          cells,
          0
        );

        var transform = new Transform2D(
          new Vector2(startXPosition, MICHAEL_START_Y_POSITION),
          0,
          new Vector2(1, 1),
          new Vector2(0, 0),
          new Vector2(MICHAEL_CELLS_WIDTH, MICHAEL_CELLS_HEIGHT) 
        );

        var enemySpeed = MICHAEL_MOVE_SPEED; 
        notificationCenter.Notify(new Notification(NotificationType.Sound, NotificationAction.Play,  ["sound_heehee"]))
        break;

      case 3:
        var direction = Math.floor(Math.random() * 2) + 1; 

        switch(direction){
          case 1:
            var chosenDirection = ENEMY_MOVE_DIRECTION;
            var startXPosition = ENEMY_START_X_POSITION;
            var cells = JASON_CELLS_RIGHT;
            break;

          case 2:
            var chosenDirection = ENEMY_MOVE_DIRECTION2;
            var startXPosition = ENEMY_START_X_POSITION2;
            var cells = JASON_CELLS_LEFT;
            break;
        }

        var spriteArtist = new AnimatedSpriteArtist(
          ctx,
          this.jasonSpriteSheet,
          JASON_ANIMATION_FPS,
          cells, 
          0
        );

        var transform = new Transform2D(
          new Vector2(startXPosition, JASON_START_Y_POSITION),
          0,
          new Vector2(1, 1),
          new Vector2(0, 0),
          new Vector2(JASON_CELLS_WIDTH, JASON_CELLS_HEIGHT)
        );

        var enemySpeed = JASON_MOVE_SPEED;  
        break;

      case 4:
        var direction = Math.floor(Math.random() * 2) + 1;

        switch(direction){
          case 1:
            var chosenDirection = ENEMY_MOVE_DIRECTION;
            var startXPosition = ENEMY_START_X_POSITION;
            var cells = FREDDY_CELLS_RIGHT;
            break;

          case 2:
            var chosenDirection = ENEMY_MOVE_DIRECTION2;
            var startXPosition = ENEMY_START_X_POSITION2;
            var cells = FREDDY_CELLS_LEFT;
            break;
        }

        var spriteArtist = new AnimatedSpriteArtist(
          ctx,
          this.freddySpriteSheet,
          FREDDY_ANIMATION_FPS,
          cells, 
          0
        );

        var transform = new Transform2D(
          new Vector2(startXPosition, FREDDY_START_Y_POSITION),
          0,
          new Vector2(1, 1),
          new Vector2(0, 0),
          new Vector2(FREDDY_CELLS_WIDTH, FREDDY_CELLS_HEIGHT)  
        );

        var enemySpeed = FREDDY_MOVE_SPEED; 
        break;

      case 5:
        var direction = Math.floor(Math.random() * 2) + 1;  

        switch(direction){
          case 1:
            var chosenDirection = ENEMY_MOVE_DIRECTION;
            var startXPosition = ENEMY_START_X_POSITION;
            var cells = TERMINATOR_CELLS_RIGHT;
            break;

          case 2:
            var chosenDirection = ENEMY_MOVE_DIRECTION2;
            var startXPosition = ENEMY_START_X_POSITION2;
            var cells = TERMINATOR_CELLS_LEFT;
            break;
        }

        var spriteArtist = new AnimatedSpriteArtist(
          ctx,
          this.terminatorSpriteSheet,
          TERMINATOR_ANIMATION_FPS,
          cells,
          0
        );

        var transform = new Transform2D(
          new Vector2(startXPosition, TERMINATOR_START_Y_POSITION),
          0,
          new Vector2(1, 1),
          new Vector2(0, 0),
          new Vector2(TERMINATOR_CELLS_WIDTH, TERMINATOR_CELLS_HEIGHT)
        );

        var enemySpeed = TERMINATOR_MOVE_SPEED; 
        break;

      case 6:
        var direction = Math.floor(Math.random() * 2) + 1;  

        switch(direction){
          case 1:
            var chosenDirection = ENEMY_MOVE_DIRECTION;
            var startXPosition = ENEMY_START_X_POSITION;
            var cells = SKATER_CELLS_RIGHT;
            break;

          case 2:
            var chosenDirection = ENEMY_MOVE_DIRECTION2;
            var startXPosition = ENEMY_START_X_POSITION2;
            var cells = SKATER_CELLS_LEFT;
            break;
        }

        var spriteArtist = new AnimatedSpriteArtist(
          ctx,
          this.skaterSpriteSheet,
          RUNNER_ANIMATION_FPS,
          cells,
          0
        );

        var transform = new Transform2D(
          new Vector2(startXPosition, SKATER_START_Y_POSITION),
          0,
          new Vector2(1, 1),
          new Vector2(0, 0),
          new Vector2(SKATER_CELLS_WIDTH, SKATER_CELLS_HEIGHT) 
        );

        var enemySpeed = SKATER_MOVE_SPEED;  
        break;

      case 7:
        var direction = Math.floor(Math.random() * 2) + 1; 
        switch(direction){
          case 1:
            var chosenDirection = ENEMY_MOVE_DIRECTION;
            var startXPosition = ENEMY_START_X_POSITION;
            var cells = GARFIELD_CELLS_RIGHT;
            break;

          case 2:
            var chosenDirection = ENEMY_MOVE_DIRECTION2;
            var startXPosition = ENEMY_START_X_POSITION2;
            var cells = GARFIELD_CELLS_LEFT;
            break;
        }

        var spriteArtist = new AnimatedSpriteArtist(
          ctx,
          this.garfieldSpriteSheet,
          GARFIELD_ANIMATION_FPS,
          cells, 
          0
        );

        var transform = new Transform2D(
          new Vector2(startXPosition, GARFIELD_START_Y_POSITION),
          0,
          new Vector2(1, 1),
          new Vector2(0, 0),
          new Vector2(GARFIELD_CELLS_WIDTH, GARFIELD_CELLS_HEIGHT)
        );

        var enemySpeed = GARFIELD_MOVE_SPEED;  
        break;

      case 8:
        var direction = Math.floor(Math.random() * 2) + 1;  

        switch(direction){
          case 1:
            var chosenDirection = ENEMY_MOVE_DIRECTION;
            var startXPosition = ENEMY_START_X_POSITION;
            var cells = CREEPER_CELLS_RIGHT;
            break;

          case 2:
            var chosenDirection = ENEMY_MOVE_DIRECTION2;
            var startXPosition = ENEMY_START_X_POSITION2;
            var cells = CREEPER_CELLS_LEFT;
            break;
        }

        var spriteArtist = new AnimatedSpriteArtist(
          ctx,
          this.creeperSpriteSheet,
          CREEPER_ANIMATION_FPS,
          cells, 
          0
        );

        var transform = new Transform2D(
          new Vector2(startXPosition, CREEPER_START_Y_POSITION),
          0,
          new Vector2(1, 1),
          new Vector2(0, 0),
          new Vector2(CREEPER_CELLS_WIDTH, CREEPER_CELLS_HEIGHT) 
        );

        var enemySpeed = CREEPER_MOVE_SPEED;
        break;

      case 9:
        var direction = Math.floor(Math.random() * 2) + 1;

        switch(direction){
          case 1:
            var chosenDirection = ENEMY_MOVE_DIRECTION;
            var startXPosition = ENEMY_START_X_POSITION;
            var cells = JOKER_CELLS_RIGHT;
            break;

          case 2:
            var chosenDirection = ENEMY_MOVE_DIRECTION2;
            var startXPosition = ENEMY_START_X_POSITION2;
            var cells = JOKER_CELLS_LEFT;
            break;
        }

        var spriteArtist = new AnimatedSpriteArtist(
          ctx,
          this.jokerSpriteSheet,
          JOKER_ANIMATION_FPS,
          cells, 
          0
        );

        var transform = new Transform2D(
          new Vector2(startXPosition, JOKER_START_Y_POSITION),
          0,
          new Vector2(1, 1),
          new Vector2(0, 0),
          new Vector2(JOKER_CELLS_WIDTH, JOKER_CELLS_HEIGHT)
        );

        var enemySpeed = JOKER_MOVE_SPEED; 
        break;

      case 10:
        var direction = Math.floor(Math.random() * 2) + 1;  

        switch(direction){
          case 1:
            var chosenDirection = ENEMY_MOVE_DIRECTION;
            var startXPosition = ENEMY_START_X_POSITION;
            var cells = SCORPION_CELLS_RIGHT;
            break;

          case 2:
            var chosenDirection = ENEMY_MOVE_DIRECTION2;
            var startXPosition = ENEMY_START_X_POSITION2;
            var cells = SCORPION_CELLS_LEFT;
            break;
        }

        var spriteArtist = new AnimatedSpriteArtist(
          ctx,
          this.scorpionSpriteSheet,
          SCORPION_ANIMATION_FPS,
          cells, 
          0
        );

        var transform = new Transform2D(
          new Vector2(startXPosition, SCORPION_START_Y_POSITION),
          0,
          new Vector2(1, 1),
          new Vector2(0, 0),
          new Vector2(SCORPION_CELLS_WIDTH, SCORPION_CELLS_HEIGHT)
        );

        var enemySpeed = SCORPION_MOVE_SPEED; 
        break;

  }

  var enemySprite = new Sprite(  //start initializing the chosen enemy
    "enemy",
    ActorType.Enemy,
    transform,
    spriteArtist,
    StatusType.IsUpdated | StatusType.IsDrawn
  );

  enemySprite.AttachBehavior(new MoveBehavior(chosenDirection, enemySpeed));

  this.objectManager.Add(enemySprite); //add enemy sprite

}

function LoadPickups() {
  var pickup = Math.floor(Math.random() * 16) + 1;

  switch(pickup) {  //Chooses a random pickup to spawn

    case 1:
      var pickupCells = CAKE_CELLS;
      break;
    case 2:
      var pickupCells = DONUT_CELLS;
      break;
    case 3:
      var pickupCells = CANDY_CELLS;
      break;
    case 4:
      var pickupCells = ICECREAM_CELLS;
      break;
    case 5:
      var pickupCells = ICECREAM2_CELLS;
      break;
    case 6:
      var pickupCells = LOLLIPOP_CELLS;
      break;
    case 7:
      var pickupCells = ICECREAM3_CELLS;
      break;
    case 8:
      var pickupCells = BISCUIT_CELLS;
      break;
    case 9:
      var pickupCells = BISCUITS_CELLS;
      break;
    case 10:
      var pickupCells = CHOCDESSERT_CELLS;
      break;
    case 11:
      var pickupCells = PANCAKE_CELLS;
      break;
    case 12:
      var pickupCells = CREAMDESSERT_CELLS;
      break;
    case 13:
      var pickupCells = CHOCDESSERT2_CELLS;
      break;
    case 14:
      var pickupCells = SUNDAE_CELLS;
      break;
    case 15:
      var pickupCells = SUNDAE2_CELLS;
      break;
    case 16:
      var pickupCells = CHERRY_CELLS;
      break;
  }


  var spriteArtist = new AnimatedSpriteArtist(ctx, this.pickupSpriteSheet,
    1, pickupCells);

    var transform = new Transform2D(new Vector2(Math.random()*(cvs.clientWidth - PICKUP_CELLS_WIDTH),0), 0,
    new Vector2(1,1), new Vector2(0,0),
    new Vector2(PICKUP_CELLS_WIDTH,PICKUP_CELLS_HEIGHT));

    var spriteArchetype = new Sprite("pickup1", ActorType.Pickup,
    transform, spriteArtist, StatusType.IsUpdated | StatusType.IsDrawn);

    spriteArchetype.AttachBehavior(new MoveBehavior(PICKUP_MOVE_DIRECTION, PICKUP_MOVE_SPEED));
    this.objectManager.Add(spriteArchetype);

}

function LoadPlayer() {
  var spriteArtist = new AnimatedSpriteArtist(
    ctx,
    this.spriteSheet,
    RUNNER_ANIMATION_FPS,
    RUNNER_CELLS_RIGHT, //used to access animation sprites for right walk
    0
  );

  var transform = new Transform2D(
    new Vector2(RUNNER_START_X_POSITION, RUNNER_START_Y_POSITION),
    0,
    new Vector2(1, 1),
    new Vector2(0, 0),
    new Vector2(RUNNER_CELLS_WIDTH, RUNNER_CELLS_HEIGHT)  //used for CD/CR rectangle 
  );

  var playerSprite = new Sprite(
    "player",
    ActorType.Player,
    transform,
    spriteArtist,
    StatusType.IsUpdated | StatusType.IsDrawn
  );

  playerSprite.AttachBehavior(
    new CAPlayerMoveBehavior(this.keyboardManager, this.objectManager,
      RUNNER_MOVE_DIRECTION, RUNNER_MOVE_SPEED,
      RUNNER_FALL_SPEED, GravityType.NormalGravity,
      RUNNER_JUMP_DURATION_IN_MS, RUNNER_MAX_JUMP_HEIGHT,
      spriteArtist, RUNNER_CELLS_LEFT, RUNNER_CELLS_RIGHT, RUNNER_CELLS_JUMP)
  );

  this.objectManager.Add(playerSprite); //add player sprite
}
// #endregion

var sounds = {
  49 : "sound_background3",
  50 : "sound_background",
  51 : "sound_background2",
};
document.onkeydown = function(e) {
  var soundId = sounds[e.keyCode];
  if (soundId) document.getElementById(soundId).play();
}


