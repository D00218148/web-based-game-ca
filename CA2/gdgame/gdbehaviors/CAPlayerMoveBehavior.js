/**
 * Moves the parent sprite based on keyboard input.
 * @author
 * @version 1.0
 * @class PlayerMoveBehavior
 */
class CAPlayerMoveBehavior {
    //fields
    keyboardManager;
    moveDirection;
    moveSpeedMultiplier;
    velocity;
    parentSpriteArtist;
    leftCells;
    rightCells;
    jumpCells;

    constructor(keyboardManager, objectManager,
        moveDirection,
        moveSpeedMultiplier,
        fallSpeedMultiplier, gravityType,
        jumpDurationInMs, maxJumpHeight,
        parentSpriteArtist, leftCells, rightCells, jumpCells) {

        this.keyboardManager = keyboardManager;
        this.objectManager = objectManager;

        this.moveDirection = moveDirection;
        this.moveSpeedMultiplier = moveSpeedMultiplier;

        this.fallSpeedMultiplier = fallSpeedMultiplier;
        this.gravityType = gravityType;

        this.jumpDurationInMs = jumpDurationInMs;
        this.maxJumpHeight = maxJumpHeight;

        this.parentSpriteArtist = parentSpriteArtist;
        this.leftCells = leftCells;
        this.rightCells = rightCells;
        this.jumpCells = jumpCells;

        this.InitializeJumpAndFallTimers();
    }

    InitializeJumpAndFallTimers() {

        this.ascendTimer = new AnimationTimer(
            this.jumpDurationInMs / 2,
            AnimationTimer.MakeEaseOutEasingFunction(1.1)
        );

        this.descendTimer = new AnimationTimer(
            this.jumpDurationInMs / 2,
            AnimationTimer.MakeEaseInEasingFunction(1.1)
        );

        this.fallTimer = new AnimationTimer();
        this.falling = false;
    }

    Execute(gameTime, parent) {
        this.HandleKeyboardInput(gameTime, parent);
        this.ExecuteJump(gameTime, parent);
    }

    HandleKeyboardInput(gameTime, parent) {

        //left
        if (this.keyboardManager.IsKeyDown(Keys.A)) {
            //set correct animation cells
            this.parentSpriteArtist.Cells = this.leftCells;

            this.velocity = -gameTime.ElapsedTimeInMs * this.moveSpeedMultiplier;
            let translateBy = Vector2.MultiplyScalar(
                this.moveDirection,
                this.velocity
            );
            parent.Transform2D.TranslateBy(translateBy);
        }

        //right
        else if (this.keyboardManager.IsKeyDown(Keys.D)) {
            //set correct animation cells
            this.parentSpriteArtist.Cells = this.rightCells;

            this.velocity = gameTime.ElapsedTimeInMs * this.moveSpeedMultiplier;
            let translateBy = Vector2.MultiplyScalar(
                this.moveDirection,
                this.velocity
            );
            parent.Transform2D.TranslateBy(translateBy);
        }

        //Jump
        if (this.keyboardManager.IsKeyDown(Keys.Space)) {
            //this.parentSpriteArtist.Cells = this.jumpCells;
            this.Jump(gameTime, parent);
            this.parentSpriteArtist.Reset();
        }
    }

    Jump(gameTime, parent) {
        if (this.jumping) return;

        this.jumping = true;
        //this.parentSpriteArtist.Pause(); // Freeze the runner while jumping
        this.verticalLaunchPosition = parent.Transform2D.Translation.Y;
        this.ascendTimer.Start(gameTime);
    }

    StopJumping(gameTime) {
        this.descendTimer.Stop(gameTime);
        this.parentSpriteArtist.Unpause(); // Restore animation
        this.jumping = false;
    }

    ExecuteJump(gameTime, parent) {
        if (!this.jumping) {
            return;
        }

        if (this.IsAscending()) {
            if (!this.IsDoneAscending(gameTime))
                this.Ascend(gameTime, parent);
            else
                this.FinishAscent(gameTime, parent);
        } else if (this.IsDescending()) {
            if (!this.IsDoneDescending(gameTime))
                this.Descend(gameTime, parent);
            else
                this.FinishDescent(gameTime, parent);
        }
    }

    IsAscending() {
        return this.ascendTimer.IsRunning();
    }

    Ascend(gameTime, parent) {
        let elapsed = this.ascendTimer.GetElapsedTime(gameTime);
        let deltaY = (elapsed / (this.jumpDurationInMs / 2)) * this.maxJumpHeight;
        parent.Transform2D.SetTranslationY(this.verticalLaunchPosition - deltaY); // Moving up
    }

    IsDoneAscending(gameTime) {
        return this.ascendTimer.GetElapsedTime(gameTime) > this.jumpDurationInMs / 2;
    }

    FinishAscent(gameTime, parent) {
        this.actualMaxJumpHeight = parent.Transform2D.Translation.Y;
        this.ascendTimer.Stop(gameTime);
        this.descendTimer.Start(gameTime);
    }

    IsDescending() {
        return this.descendTimer.IsRunning();
    }

    Descend(gameTime, parent) {
        let elapsed = this.descendTimer.GetElapsedTime(gameTime);
        let deltaY = (elapsed / (this.jumpDurationInMs / 2)) * this.maxJumpHeight;
        parent.Transform2D.SetTranslationY(this.actualMaxJumpHeight + deltaY); //Moving down
    }

    IsDoneDescending(gameTime) {
        return this.descendTimer.GetElapsedTime(gameTime) > this.jumpDurationInMs / 2;
    }

    FinishDescent(gameTime, parent) {
        this.StopJumping(gameTime);

        let platformY = this.PlatformUnderneath(parent);
        if (platformY != -1) {
            parent.Transform2D.SetTranslationY(this.verticalLaunchPosition); //
        } else {
            let initialFallVelocity = this.gravityType * (this.descendTimer.GetElapsedTime(gameTime) / 1000) * this.fallSpeedMultiplier;
            this.StartFalling(gameTime, initialFallVelocity);
        }
    }

    StartFalling(gameTime, initialFallVelocity) {
        this.falling = true;
        this.initialFallVelocity = initialFallVelocity || 0;
        this.fallVelocity = initialFallVelocity || 0;
        this.fallTimer.Start(gameTime);
    };

    StopFalling(gameTime) {
        this.falling = false;
        this.initialFallVelocity = 0;
        this.fallVelocity = 0;
        this.fallTimer.Stop(gameTime);
    };

    PlatformUnderneath(parent) {

        for (var i = 0; i < this.objectManager.PlatformSprites.length; ++i) {
            let sprite = this.objectManager.PlatformSprites[i];
            let boundingBox = sprite.Transform2D.BoundingBox;

            if (parent.Transform2D.BoundingBox.IsOnTop(boundingBox))
                return boundingBox.Y;
        }

        return -1;
    }

    SetSpriteVelocity(gameTime) {
        this.fallVelocity = this.initialFallVelocity + this.gravityType * this.fallTimer.GetElapsedTime(gameTime) / 1000 * this.fallSpeedMultiplier;
    };

    CalculateVerticalDrop(gameTime) {
        return this.fallVelocity * gameTime.ElapsedTimeInMs / 1000;
    };

    Clone() {
        return new CAPlayerMoveBehavior(
            this.keyboardManager, this.moveDirection, this.moveSpeedMultiplier,
            this.jumpDurationInMs, this.maxJumpHeight,
            this.parentSpriteArtist, this.leftCells, this.rightCells
        );
    }
}