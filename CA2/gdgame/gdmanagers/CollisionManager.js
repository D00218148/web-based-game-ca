class CollisionManager{

    constructor(objectManager, soundManager, screenRectangle){
        this.objectManager = objectManager;
        this.soundManager = soundManager;
        this.screenRectangle = screenRectangle;
        //this.boundaryRectangle = boundaryRectangle;
        var timeOfCollision;
        var enemy;
    }

    Update(gameTime){
        this.HandleHitEnemy(gameTime);
        this.HandleHitPickup(gameTime);
        this.CheckPlayerBounds(gameTime);
    }

    CheckPlayerBounds(gameTime)
    {
        for(let i = 0; i < this.objectManager.PlayerSprites.length; i++)
        {
            let sprite = this.objectManager.PlayerSprites[i];
            let boundingBox = sprite.Transform2D.BoundingBox;

            if(!boundingBox.Intersects(this.screenRectangle))
                sprite.position = pos;
        }
    }

    CheckBulletBounds(gameTime)
    {
        for(let i = 0; i < this.objectManager.BulletSprites.length; i++)
        {
            let sprite = this.objectManager.BulletSprites[i];
            let boundingBox = sprite.Transform2D.BoundingBox;

            if(!boundingBox.Intersects(this.screenRectangle))
                this.objectManager.BulletSprites.splice(i, 1);
        }
    }

    CheckPickupBounds(gameTime)
    {
        for(let i = 0; i < this.objectManager.PickupSprites.length; i++)
        {
            let sprite = this.objectManager.PickupSprites[i];
            let boundingBox = sprite.Transform2D.BoundingBox;

            if(!boundingBox.Intersects(this.screenRectangle))
                this.objectManager.PickupSprites.splice(i, 1);
        }
    }

    HandleHitEnemy(gameTime)
    {
        for(let i = 0; i < this.objectManager.PlayerSprites.length; i++)
        {
            let sprite = this.objectManager.PlayerSprites[i];
            let boundingBox = sprite.Transform2D.BoundingBox;

            for(let j = 0; j < this.objectManager.EnemySprites.length; j++)
            {
                let enemy = this.objectManager.EnemySprites[j];
                let enemyBoundingBox = enemy.Transform2D.BoundingBox;

                if(boundingBox.Intersects(enemyBoundingBox))
                {
                    if(gameTime.TotalElapsedTimeInMs - this.timeOfCollision > 2000 || this.timeOfCollision == undefined)
                    {
                        this.timeOfCollision = gameTime.TotalElapsedTimeInMs;
                        notificationCenter.Notify(new Notification(NotificationType.Sound, NotificationAction.Play,  ["sound_zoinks"]));
                        this.objectManager.health -= 1;
                    }

                    if(this.objectManager.health == 0)
                    {
                        notificationCenter.Notify(new Notification(NotificationType.Game, NotificationAction.GameOver, [StatusType.IsDrawn]));
                        ctx.fillStyle = "Red";
                        ctx.font      = "bold 30pt Comic Sans";
                        ctx.fillText("Game Over", 280, 330);
                        window.setInterval(this.ReloadPage, 2000);
                        ReloadPage();
                    }
                    
                }
            }
        }
    }

    ReloadPage()
    {
        document.location.reload();
    }

    
    HandleHitPickup(gameTime)
    {
        for(let i = 0; i < this.objectManager.PlayerSprites.length; i++)
        {
            let player = this.objectManager.PlayerSprites[i];
            let boundingBox = player.Transform2D.BoundingBox;

            for(let j = 0; j < this.objectManager.PickupSprites.length; j++)
            {
                let pickup = this.objectManager.PickupSprites[j];
                let pickupBoundingBox = pickup.Transform2D.BoundingBox;

                if(boundingBox.Intersects(pickupBoundingBox))
                {
                    this.objectManager.PickupSprites.splice(i, 1);
                    this.objectManager.score += 10;
                    notificationCenter.Notify(new Notification(NotificationType.Sound, NotificationAction.Play, ["sound_chomp"]));
                }
            }
        }
    }


}