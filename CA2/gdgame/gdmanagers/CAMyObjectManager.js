/**
 * Stores, updates, and draws all sprites in game
 * @author
 * @version 1.0
 * @class MyObjectManager
 */

class CAMyObjectManager extends ObjectManager {

    //PC and NPCs
    enemySprites = [];
    playerSprites = [];
    platformSprites = [];
    pickupSprites = [];
    score = 0;
    health = 3;

    //stores all other sprites e.g. background
    decoratorSprites = [];
    backgroundSprites = [];

    //this offset will be applied to all sprites listed in UpdateGlobalTranslationOffset()
    deltaTranslationOffset = Vector2.Zero;

    //these getters allow a collision manager to get access to the sprites for collision detection/collision response (CD/CR) testing
    get EnemySprites() { return this.enemySprites;}
    get PickupSprites() { return this.pickupSprites;}
    get PlayerSprites() { return this.playerSprites;}
    get PlatformSprites() { return this.platformSprites;}

    /**
     * Used by a player sprite to set the translation offset for all sprites that need to be moved when player moves.
     * e.g. in a scrollable game we move everything around the player instead of moving the player.     *
     * @memberof CAMyObjectManager
     */
    set DeltaTranslationOffset(deltaTranslationOffset) {this.deltaTranslationOffset = deltaTranslationOffset;}

    constructor(id, ctx=null, debugEnabled=false, notificationCenter) {
        super(id, ctx, debugEnabled);
        this.notificationCenter = notificationCenter;
        this.RegisterForNotifications();
    }

    //#region Notification Handling

  //handle all GameState type events
  RegisterForNotifications() {

    //handle events related to add/remove sprites
    this.notificationCenter.Register(
      NotificationType.Sprite,
      this,
      this.HandleNotification
    );

    this.notificationCenter.Register(
      NotificationType.Menu,
      this,
      this.HandleNotification
    );


    this.notificationCenter.Register(
        NotificationType.Game,
        this,
        this.HandleNotification
      );


  }

  HandleNotification(...argArray) {
    let notification = argArray[0];
    switch (notification.NotificationAction) {
      case NotificationAction.Remove:
        this.HandleRemoveSprite(notification.NotificationArguments);
        break;

      case NotificationAction.Spawn:
        this.HandleSpawnSprite(notification.NotificationArguments);
        break;

      case NotificationAction.ShowMenuChanged:
        this.HandleShowMenu(notification.NotificationArguments);
        break;

       case NotificationAction.GameOver:
        this.HandleGameOver(notification.NotificationArguments);
         break;

      default:
        break;
    }
  }

  HandleGameOver(argArray) {
    this.statusType = argArray[0];
  }

  HandleShowMenu(argArray) {
    this.statusType = argArray[0];
  }

    Add(sprite) {
        //is it valid and the correct object type?
        if (sprite != null && sprite instanceof Sprite) {
            //does it have a sprite type?
            if (sprite.ActorType) {
                switch (sprite.ActorType) {
                    case ActorType.Enemy:
                        this.enemySprites.push(sprite);
                        break;
                    case ActorType.Player:
                        this.playerSprites.push(sprite);
                        break;
                    case ActorType.Platform:   //new for Snailbait
                        this.platformSprites.push(sprite);
                        break;
                    case ActorType.Background:
                        this.backgroundSprites.push(sprite);
                        break;
                    case ActorType.Pickup:
                        this.pickupSprites.push(sprite);
                        break;
                    default:
                        this.decoratorSprites.push(sprite);
                        break;
                }
            }
        }
    }

    Update(gameTime) {
        if ((this.statusType & StatusType.IsUpdated) != 0) {
            this.UpdateTranslationOffset(gameTime);
            this.UpdateAll(gameTime);
        }
    }

    Draw(gameTime) {
        if ((this.statusType & StatusType.IsDrawn) != 0) {
            this.DrawAll(gameTime);
        if (this.DebugEnabled)
            this.DrawDebug("red", "green", "white", "yellow");
        }
    }

    /**
     * Call this to re-position all the sprites that move around the player sprite i.e. to enable side-scrolling.
     *
     * @param {GameTime} gameTime
     * @memberof MyObjectManager
     */
    UpdateTranslationOffset(gameTime)
    {
        //add to each sprites existing translation offset if non-zero
        if(this.deltaTranslationOffset.Length() != 0)
        {
            for (let i = 0; i < this.enemySprites.length; i++)
                this.enemySprites[i].Transform2D.AddToTranslationOffset(this.deltaTranslationOffset);

            for (let i = 0; i < this.platformSprites.length; i++)
                this.platformSprites[i].Transform2D.AddToTranslationOffset(this.deltaTranslationOffset);

            for (let i = 0; i < this.decoratorSprites.length; i++)
                this.decoratorSprites[i].Transform2D.AddToTranslationOffset(this.deltaTranslationOffset);

            //set the delta back to zero, otherwise it will keep being applied in each Update()
            this.deltaTranslationOffset = Vector2.Zero;
        }
    }

    UpdateAll(gameTime){

        for (let i = 0; i < this.enemySprites.length; i++)
            this.enemySprites[i].Update(gameTime);

        for (let i = 0; i < this.playerSprites.length; i++)
            this.playerSprites[i].Update(gameTime);

        for (let i = 0; i < this.platformSprites.length; i++)
            this.platformSprites[i].Update(gameTime);

        for (let i = 0; i < this.decoratorSprites.length; i++)
            this.decoratorSprites[i].Update(gameTime);

        for (let i = 0; i < this.backgroundSprites.length; i++)
            this.backgroundSprites[i].Update(gameTime);

        for (let i = 0; i < this.pickupSprites.length; i++)
            this.pickupSprites[i].Update(gameTime);
    }

    DrawAll(gameTime){
        for (let i = 0; i < this.backgroundSprites.length; i++)
            this.backgroundSprites[i].Draw(gameTime);

        for (let i = 0; i < this.decoratorSprites.length; i++)
            this.decoratorSprites[i].Draw(gameTime);

        for (let i = 0; i < this.enemySprites.length; i++)
            this.enemySprites[i].Draw(gameTime);

        for (let i = 0; i < this.pickupSprites.length; i++)
            this.pickupSprites[i].Draw(gameTime);

        for (let i = 0; i < this.platformSprites.length; i++)
            this.platformSprites[i].Draw(gameTime);

        for (let i = 0; i < this.playerSprites.length; i++)
            this.playerSprites[i].Draw(gameTime);
    }

//#region Debug
    //draws the CD/CR Rect around sprites if enabled
    DrawDebug(debugEnemyColor, debugPlayerColor, debugPlatformColor, debugPickupColor){

    for (let i = 0; i < this.enemySprites.length; i++)
        this.DrawDebugBoundingBox(debugEnemyColor, this.enemySprites[i].Transform2D.BoundingBox);

    for (let i = 0; i < this.playerSprites.length; i++)
        this.DrawDebugBoundingBox(debugPlayerColor, this.playerSprites[i].Transform2D.BoundingBox);

    for (let i = 0; i < this.platformSprites.length; i++)
        this.DrawDebugBoundingBox(debugPlatformColor, this.platformSprites[i].Transform2D.BoundingBox);

    for (let i = 0; i < this.pickupSprites.length; i++)
        this.DrawDebugBoundingBox(debugPickupColor, this.pickupSprites[i].Transform2D.BoundingBox);
    }


//#endregion

}