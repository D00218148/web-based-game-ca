
/********************************************************************** AUDIO RESOURCES *********************************************************************/
const audioCueArray = [
	//add game specific audio cues here...
	
	new AudioCue("sound_background",
		AudioType.Background, 1, 1, false, 0),
   new AudioCue("sound_background2",
      AudioType.Background, 1, 1, false, 0),
   new AudioCue("sound_background3",
      AudioType.Background, 1, 1, false, 0),
   new AudioCue("sound_zoinks",
      AudioType.Hit, 1, 1, false, 0),
   new AudioCue("sound_chomp",
      AudioType.Hit, 1, 1, false, 0),
      
   new AudioCue("sound_heehee",
   AudioType.Background, 1, 1, false, 0)
	
];

/*********************************************************************************************************************************************************/

//#region  Sprite sheet cells

   const RUNNER_CELLS_WIDTH = 50; // in pixels
   const RUNNER_CELLS_HEIGHT = 52;
   const RUNNER_CELLS_HEIGHT_JUMP = 70;
   const RUNNER_ANIMATION_FPS = 12;
   const RUNNER_START_X_POSITION = 350;
   const RUNNER_START_Y_POSITION = 530;
   const RUNNER_MOVE_DIRECTION = new Vector2(1, 0);
   const RUNNER_MOVE_SPEED = 0.2;
   const RUNNER_JUMP_DURATION_IN_MS = 1000;
   const RUNNER_MAX_JUMP_HEIGHT = 100;
   const RUNNER_FALL_SPEED = 10;

   const ENEMY_MOVE_DIRECTION = new Vector2(1, 0);
   const ENEMY_MOVE_DIRECTION2 = new Vector2(-1, 0);
   const ENEMY_START_X_POSITION = -50;
   const ENEMY_START_X_POSITION2 = 849;

   const STRIPE_CELLS_HEIGHT = 52;
   const STRIPE_CELLS_WIDTH = 50;
   const STRIPE_START_Y_POSITION = 530;
   const STRIPE_MOVE_SPEED = 0.35;

   const SKATER_CELLS_HEIGHT = 59;
   const SKATER_CELLS_WIDTH = 79;
   const SKATER_START_Y_POSITION = 530;
   const SKATER_MOVE_SPEED = 0.4;

   const MICHAEL_CELLS_HEIGHT = 62;
   const MICHAEL_CELLS_WIDTH = 32;
   const MICHAEL_ANIMATION_FPS = 8;
   const MICHAEL_START_Y_POSITION = 518;
   const MICHAEL_MOVE_SPEED = 0.3;

   const JASON_CELLS_HEIGHT = 65;
   const JASON_CELLS_WIDTH = 27;
   const JASON_ANIMATION_FPS = 8;
   const JASON_START_Y_POSITION = 518;
   const JASON_MOVE_SPEED = 0.15;

   const FREDDY_CELLS_HEIGHT = 66;
   const FREDDY_CELLS_WIDTH = 53;
   const FREDDY_ANIMATION_FPS = 8;
   const FREDDY_START_Y_POSITION = 525;
   const FREDDY_MOVE_SPEED = 0.25;

   const TERMINATOR_CELLS_HEIGHT = 74;
   const TERMINATOR_CELLS_WIDTH = 63;
   const TERMINATOR_ANIMATION_FPS = 12;
   const TERMINATOR_START_Y_POSITION = 510;
   const TERMINATOR_MOVE_SPEED = 0.2;

   const GARFIELD_CELLS_HEIGHT = 51;
   const GARFIELD_CELLS_WIDTH = 57;
   const GARFIELD_ANIMATION_FPS = 12;
   const GARFIELD_START_Y_POSITION = 535;
   const GARFIELD_MOVE_SPEED = 0.34;

   const CREEPER_CELLS_HEIGHT = 55;
   const CREEPER_CELLS_WIDTH = 94;
   const CREEPER_ANIMATION_FPS = 8;
   const CREEPER_START_Y_POSITION = 530;
   const CREEPER_MOVE_SPEED = 0.06;

   const JOKER_CELLS_HEIGHT = 45;
   const JOKER_CELLS_WIDTH = 48;
   const JOKER_ANIMATION_FPS = 8;
   const JOKER_START_Y_POSITION = 535;
   const JOKER_MOVE_SPEED = 0.28;

   const SCORPION_CELLS_HEIGHT = 79;
   const SCORPION_CELLS_WIDTH = 41;
   const SCORPION_ANIMATION_FPS = 8;
   const SCORPION_START_Y_POSITION = 510;
   const SCORPION_MOVE_SPEED = 0.32;
  
   const PICKUP_CELLS_HEIGHT = 35;
   const PICKUP_CELLS_WIDTH  = 42; 
   const PICKUP_MOVE_DIRECTION = new Vector2(0, 1);
   const PICKUP_MOVE_SPEED = 0.25;

   const BACKGROUND_CELL = [
      { left: 0,   top: 590, width: 1104, height: 400 }
   ];

   const CAKE_CELLS = [
      { left: 5, top: 411, width:  PICKUP_CELLS_WIDTH, 
         height: PICKUP_CELLS_HEIGHT }
   ];

   const DONUT_CELLS = [
      { left: 71, top: 411, width:  PICKUP_CELLS_WIDTH, 
         height: PICKUP_CELLS_HEIGHT }
   ];

   const CANDY_CELLS = [
      { left: 131, top: 403, width:  PICKUP_CELLS_WIDTH, 
         height: PICKUP_CELLS_HEIGHT }
   ];

   const ICECREAM_CELLS = [
      { left: 202, top: 411, width:  PICKUP_CELLS_WIDTH, 
         height: PICKUP_CELLS_HEIGHT }
   ];

   const ICECREAM2_CELLS = [
      { left: 266, top: 411, width:  PICKUP_CELLS_WIDTH, 
         height: PICKUP_CELLS_HEIGHT }
   ];

   const LOLLIPOP_CELLS = [
      { left: 326, top: 411, width:  PICKUP_CELLS_WIDTH, 
         height: PICKUP_CELLS_HEIGHT }
   ];

   const ICECREAM3_CELLS = [
      { left: 393, top: 411, width:  PICKUP_CELLS_WIDTH, 
         height: PICKUP_CELLS_HEIGHT }
   ];

   const BISCUIT_CELLS = [
      { left: 457, top: 411, width:  PICKUP_CELLS_WIDTH, 
         height: PICKUP_CELLS_HEIGHT }
   ];

   const BISCUITS_CELLS = [
      { left: 1, top: 469, width:  PICKUP_CELLS_WIDTH, 
         height: PICKUP_CELLS_HEIGHT }
   ];

   const CHOCDESSERT_CELLS = [
      { left: 69, top: 477, width:  PICKUP_CELLS_WIDTH, 
         height: PICKUP_CELLS_HEIGHT }
   ];

   const PANCAKE_CELLS = [
      { left: 131, top: 477, width:  PICKUP_CELLS_WIDTH, 
         height: PICKUP_CELLS_HEIGHT }
   ];

   const CREAMDESSERT_CELLS = [
      { left: 198, top: 477, width:  PICKUP_CELLS_WIDTH, 
         height: PICKUP_CELLS_HEIGHT }
   ];

   const CHOCDESSERT2_CELLS = [
      { left: 264, top: 477, width:  PICKUP_CELLS_WIDTH, 
         height: PICKUP_CELLS_HEIGHT }
   ];

   const SUNDAE_CELLS = [
      { left: 326, top: 477, width:  PICKUP_CELLS_WIDTH, 
         height: PICKUP_CELLS_HEIGHT }
   ];

   const SUNDAE2_CELLS = [
      { left: 393, top: 477, width:  PICKUP_CELLS_WIDTH, 
         height: PICKUP_CELLS_HEIGHT }
   ];

   const CHERRY_CELLS = [
      { left: 67, top: 19, width:  PICKUP_CELLS_WIDTH, 
         height: PICKUP_CELLS_HEIGHT }
   ];

   const RUNNER_CELLS_RIGHT = [
      { left: 0, top: 348, 
        width: 58, height: RUNNER_CELLS_HEIGHT },

      { left: 58, top: 348, 
         width: 55, height: RUNNER_CELLS_HEIGHT },

      { left: 114, top: 348, 
         width: 47, height: RUNNER_CELLS_HEIGHT },

      { left: 162, top: 348, 
         width: 43, height: RUNNER_CELLS_HEIGHT },

      { left: 206, top: 348, 
         width: 51, height: RUNNER_CELLS_HEIGHT },

      { left: 258, top: 348, 
         width: 61, height: RUNNER_CELLS_HEIGHT },

      { left: 320,  top: 348, 
         width: 60, height: RUNNER_CELLS_HEIGHT },

      { left: 383,  top: 348, 
         width: 53, height: RUNNER_CELLS_HEIGHT },

      { left: 437,   top: 348, 
         width: 44, height: RUNNER_CELLS_HEIGHT },

      { left: 482,   top: 348, 
         width: 50, height: RUNNER_CELLS_HEIGHT }
   ];

   const RUNNER_CELLS_LEFT = [
      { left: 474,   top: 150, 
         width: 56, height: RUNNER_CELLS_HEIGHT },

      { left: 419,  top: 150, 
         width: 54, height: RUNNER_CELLS_HEIGHT },

      { left: 371, top: 150, 
         width: 47, height: RUNNER_CELLS_HEIGHT },

      { left: 326, top: 150, 
         width: 44, height: RUNNER_CELLS_HEIGHT },

      { left: 274, top: 150, 
         width: 51, height: RUNNER_CELLS_HEIGHT },

      { left: 212, top: 150, 
         width: 61, height: RUNNER_CELLS_HEIGHT },

      { left: 151, top: 150, 
         width: 60, height: RUNNER_CELLS_HEIGHT },

      { left: 96, top: 150, 
         width: 54, height: RUNNER_CELLS_HEIGHT },

      { left: 51, top: 150, 
         width: 44, height: RUNNER_CELLS_HEIGHT },

      { left: 0, top: 150, 
         width: 50, height: RUNNER_CELLS_HEIGHT }
   ];

   const RUNNER_CELLS_JUMP = [
      { left: 0,   top: 205, 
         width: 27, height: RUNNER_CELLS_HEIGHT_JUMP },

      { left: 23,  top: 205, 
         width: 34, height: RUNNER_CELLS_HEIGHT_JUMP },

      { left: 53, top: 205, 
         width: 27, height: RUNNER_CELLS_HEIGHT_JUMP },

      { left: 81, top: 205, 
         width: 23, height: RUNNER_CELLS_HEIGHT_JUMP },

      { left: 103, top: 205, 
         width: 29, height: RUNNER_CELLS_HEIGHT_JUMP },

      { left: 131, top: 205, 
         width: 32, height: RUNNER_CELLS_HEIGHT_JUMP },

      { left: 160, top: 205, 
         width: 37, height: RUNNER_CELLS_HEIGHT_JUMP },

      { left: 197, top: 205, 
         width: 33, height: RUNNER_CELLS_HEIGHT_JUMP },

      { left: 228, top: 205, 
         width: 23, height: RUNNER_CELLS_HEIGHT_JUMP }
   ];

   const STRIPE_CELLS_RIGHT = [
      { left: 2, top: 73, 
        width: 49, height: STRIPE_CELLS_HEIGHT },

      { left: 55, top: 73, 
         width: 47, height: STRIPE_CELLS_HEIGHT },

      { left: 105, top: 73, 
         width: 45, height: STRIPE_CELLS_HEIGHT },

      { left: 154, top: 73, 
         width: 47, height: STRIPE_CELLS_HEIGHT }
   ];

   const STRIPE_CELLS_LEFT = [
      { left: 154, top: 2, 
         width: 50, height: STRIPE_CELLS_HEIGHT },
 
       { left: 103, top: 2, 
          width: 48, height: STRIPE_CELLS_HEIGHT },
 
       { left: 55, top: 2, 
          width: 46, height: STRIPE_CELLS_HEIGHT },
 
       { left: 4, top: 2, 
          width: 47, height: STRIPE_CELLS_HEIGHT }
   ];

   const MICHAEL_CELLS_RIGHT = [
      { left: 1, top: 1, 
        width: 29, height: MICHAEL_CELLS_HEIGHT },

      { left: 42, top: 1, 
         width: 25, height: MICHAEL_CELLS_HEIGHT },

      { left: 77, top: 1, 
         width: 22, height: MICHAEL_CELLS_HEIGHT },

      { left: 108, top: 1, 
         width: 23, height: MICHAEL_CELLS_HEIGHT },

      { left: 137, top: 1,  
         width: 26, height: MICHAEL_CELLS_HEIGHT },

      { left: 172, top: 1,  
         width: 23, height: MICHAEL_CELLS_HEIGHT },
   
      { left: 204, top: 1, 
         width: 27, height: MICHAEL_CELLS_HEIGHT },

      { left: 244, top: 1,  
         width: 27, height: MICHAEL_CELLS_HEIGHT },
      
      { left: 283, top: 1,  
         width: 32, height: MICHAEL_CELLS_HEIGHT },

      { left: 321, top: 1, 
         width: 26, height: MICHAEL_CELLS_HEIGHT },
         
      { left: 356, top: 1, 
         width: 22, height: MICHAEL_CELLS_HEIGHT },

      { left: 388, top: 1,  
         width: 27, height: MICHAEL_CELLS_HEIGHT },
            
      { left: 428, top: 1, 
         width: 28, height: MICHAEL_CELLS_HEIGHT },

      { left: 467, top: 1, 
         width: 23, height: MICHAEL_CELLS_HEIGHT },
               
      { left: 501, top: 1, 
         width: 22, height: MICHAEL_CELLS_HEIGHT }
         
   ];

   const MICHAEL_CELLS_LEFT = [
      { left: 501, top: 1, 
         width: 22, height: MICHAEL_CELLS_HEIGHT },
 
      { left: 467, top: 1, 
         width: 23, height: MICHAEL_CELLS_HEIGHT },
 
      { left: 428, top: 1, 
         width: 28, height: MICHAEL_CELLS_HEIGHT },
 
      { left: 388, top: 1, 
         width: 27, height: MICHAEL_CELLS_HEIGHT },
 
      { left: 356, top: 1, 
         width: 22, height: MICHAEL_CELLS_HEIGHT },
 
      { left: 321, top: 1,   
         width: 26, height: MICHAEL_CELLS_HEIGHT },
    
      { left: 283, top: 1,  
         width: 32, height: MICHAEL_CELLS_HEIGHT },
 
      { left: 244, top: 1,   
         width: 27, height: MICHAEL_CELLS_HEIGHT },
       
      { left: 204, top: 1,   
         width: 27, height: MICHAEL_CELLS_HEIGHT },
 
      { left: 172, top: 1, 
         width: 23, height: MICHAEL_CELLS_HEIGHT },
          
      { left: 137, top: 1, 
         width: 26, height: MICHAEL_CELLS_HEIGHT },
 
      { left: 108, top: 1,   
         width: 23, height: MICHAEL_CELLS_HEIGHT },
             
      { left: 77, top: 1,  
         width: 22, height: MICHAEL_CELLS_HEIGHT },
 
      { left: 42, top: 1,  
         width: 25, height: MICHAEL_CELLS_HEIGHT },
                
      { left: 1, top: 1,  
         width: 29, height: MICHAEL_CELLS_HEIGHT }
   ];

   const JASON_CELLS_RIGHT = [
      { left: 0, top: 0, 
        width: 25, height: JASON_CELLS_HEIGHT },

      { left: 26, top: 0, 
         width: 20, height: JASON_CELLS_HEIGHT },

      { left: 47, top: 0, 
         width: 27, height: JASON_CELLS_HEIGHT }
   ];

   const JASON_CELLS_LEFT = [
      { left: 2, top: 66, 
         width: 26, height: JASON_CELLS_HEIGHT },
 
       { left: 29, top: 66, 
          width: 20, height: JASON_CELLS_HEIGHT },
 
       { left: 50, top: 66, 
          width: 26, height: JASON_CELLS_HEIGHT }
   ];

   const FREDDY_CELLS_RIGHT = [
      { left: 15, top: 104, 
         width: 53, height: FREDDY_CELLS_HEIGHT },
 
       { left: 87, top: 104, 
          width: 49, height: FREDDY_CELLS_HEIGHT },
 
       { left: 173, top: 104, 
          width: 41, height: FREDDY_CELLS_HEIGHT }
   ];

   const FREDDY_CELLS_LEFT = [
      { left: 15, top: 15, 
        width: 41, height: FREDDY_CELLS_HEIGHT },

      { left: 93, top: 15, 
         width: 49, height: FREDDY_CELLS_HEIGHT },

      { left: 161, top: 15, 
         width: 53, height: FREDDY_CELLS_HEIGHT }
   ];

   const TERMINATOR_CELLS_RIGHT = [
      { left: 5, top: 97, 
        width: 63, height: TERMINATOR_CELLS_HEIGHT },

      { left: 96, top: 97, 
         width: 54, height: TERMINATOR_CELLS_HEIGHT },

      { left: 174, top: 97, 
         width: 51, height: TERMINATOR_CELLS_HEIGHT },

      { left: 247, top: 97, 
         width: 50, height: TERMINATOR_CELLS_HEIGHT },

      { left: 310, top: 97,  
         width: 51, height: TERMINATOR_CELLS_HEIGHT },

      { left: 380, top: 97,  
         width: 51, height: TERMINATOR_CELLS_HEIGHT },
   
      { left: 455, top: 97, 
         width: 45, height: TERMINATOR_CELLS_HEIGHT },

      { left: 527, top: 97,  
         width: 51, height: TERMINATOR_CELLS_HEIGHT },
      
      { left: 604, top: 97,  
         width: 56, height: TERMINATOR_CELLS_HEIGHT },

      { left: 674, top: 97, 
         width: 63, height: TERMINATOR_CELLS_HEIGHT }        
   ];

   const TERMINATOR_CELLS_LEFT = [
      { left: 676, top: 9, 
         width: 63, height: TERMINATOR_CELLS_HEIGHT },
 
       { left: 594, top: 9, 
          width: 54, height: TERMINATOR_CELLS_HEIGHT },
 
       { left: 519, top: 9, 
          width: 51, height: TERMINATOR_CELLS_HEIGHT },
 
       { left: 447, top: 9, 
          width: 50, height: TERMINATOR_CELLS_HEIGHT },
 
       { left: 383, top: 9,  
          width: 51, height: TERMINATOR_CELLS_HEIGHT },
 
       { left: 313, top: 9,  
          width: 51, height: TERMINATOR_CELLS_HEIGHT },
    
       { left: 240, top: 9, 
          width: 45, height: TERMINATOR_CELLS_HEIGHT },
 
       { left: 166, top: 9,  
          width: 51, height: TERMINATOR_CELLS_HEIGHT },
       
       { left: 84, top: 9,  
          width: 56, height: TERMINATOR_CELLS_HEIGHT },
 
       { left: 7, top: 9, 
          width: 63, height: TERMINATOR_CELLS_HEIGHT }        
    ];

   const SKATER_CELLS_RIGHT = [
      { left: 7, top: 8, 
        width: 54, height: SKATER_CELLS_HEIGHT },

      { left: 64, top: 8, 
         width: 48, height: SKATER_CELLS_HEIGHT },

      { left: 112, top: 8, 
         width: 76, height: SKATER_CELLS_HEIGHT },

      { left: 188, top: 8, 
         width: 76, height: SKATER_CELLS_HEIGHT },

      { left: 265, top: 8,  
         width: 57, height: SKATER_CELLS_HEIGHT },

      { left: 328, top: 8,  
         width: 48, height: SKATER_CELLS_HEIGHT },
   
      { left: 383, top: 8, 
         width: 47, height: SKATER_CELLS_HEIGHT },

      { left: 430, top: 8,  
         width: 78, height: SKATER_CELLS_HEIGHT },
      
      { left: 508, top: 8,  
         width: 79, height: SKATER_CELLS_HEIGHT },

      { left: 587, top: 8, 
         width: 69, height: SKATER_CELLS_HEIGHT }        
   ];

   const SKATER_CELLS_LEFT = [
      { left: 603, top: 86, 
         width: 54, height: SKATER_CELLS_HEIGHT },
 
       { left: 552, top: 86, 
          width: 48, height: SKATER_CELLS_HEIGHT },
 
       { left: 476, top: 86, 
          width: 76, height: SKATER_CELLS_HEIGHT },
 
       { left: 400, top: 86, 
          width: 76, height: SKATER_CELLS_HEIGHT },
 
       { left: 342, top: 86,  
          width: 57, height: SKATER_CELLS_HEIGHT },
 
       { left: 288, top: 86,  
          width: 48, height: SKATER_CELLS_HEIGHT },
    
       { left: 234, top: 86, 
          width: 47, height: SKATER_CELLS_HEIGHT },
 
       { left: 156, top: 86,  
          width: 78, height: SKATER_CELLS_HEIGHT },
       
       { left: 77, top: 86,  
          width: 79, height: SKATER_CELLS_HEIGHT },
 
       { left: 8, top: 86, 
          width: 69, height: SKATER_CELLS_HEIGHT }        
    ];

    const GARFIELD_CELLS_RIGHT = [
      { left: 0, top: 18, 
        width: 57, height: GARFIELD_CELLS_HEIGHT },

      { left: 59, top: 18, 
         width: 56, height: GARFIELD_CELLS_HEIGHT },

      { left: 120, top: 18, 
         width: 55, height: GARFIELD_CELLS_HEIGHT },

      { left: 180, top: 18, 
         width: 55, height: GARFIELD_CELLS_HEIGHT },

      { left: 241, top: 18,  
         width: 57, height: GARFIELD_CELLS_HEIGHT },

      { left: 301, top: 18,  
         width: 54, height: GARFIELD_CELLS_HEIGHT },
   
      { left: 360, top: 18, 
         width: 55, height: GARFIELD_CELLS_HEIGHT },

      { left: 420, top: 18,  
         width: 55, height: GARFIELD_CELLS_HEIGHT }    
   ];

   const GARFIELD_CELLS_LEFT = [
      { left: 419, top: 87, 
        width: 57, height: GARFIELD_CELLS_HEIGHT },

      { left: 361, top: 87, 
         width: 56, height: GARFIELD_CELLS_HEIGHT },

      { left: 301, top: 87, 
         width: 55, height: GARFIELD_CELLS_HEIGHT },

      { left: 241, top: 87, 
         width: 55, height: GARFIELD_CELLS_HEIGHT },

      { left: 178, top: 87,  
         width: 57, height: GARFIELD_CELLS_HEIGHT },

      { left: 121, top: 87,  
         width: 54, height: GARFIELD_CELLS_HEIGHT },
   
      { left: 61, top: 87, 
         width: 55, height: GARFIELD_CELLS_HEIGHT },

      { left: 1, top: 87,  
         width: 55, height: GARFIELD_CELLS_HEIGHT }    
   ];

   const CREEPER_CELLS_RIGHT = [
      { left: 1, top: 1, 
        width: 59, height: CREEPER_CELLS_HEIGHT },

      { left: 61, top: 1, 
         width: 52, height: CREEPER_CELLS_HEIGHT },

      { left: 114, top: 1, 
         width: 58, height: CREEPER_CELLS_HEIGHT },

      { left: 172, top: 1, 
         width: 61, height: CREEPER_CELLS_HEIGHT },

      { left: 234, top: 1,  
         width: 61, height: CREEPER_CELLS_HEIGHT },

      { left: 297, top: 1,  
         width: 79, height: CREEPER_CELLS_HEIGHT },
   
      { left: 377, top: 1, 
         width: 86, height: CREEPER_CELLS_HEIGHT },

      { left: 464, top: 1,  
         width: 94, height: CREEPER_CELLS_HEIGHT },
         
      { left: 558, top: 1, 
         width: 60, height: CREEPER_CELLS_HEIGHT },
    
      { left: 619, top: 1, 
         width: 53, height: CREEPER_CELLS_HEIGHT },
    
      { left: 673, top: 1, 
         width: 57, height: CREEPER_CELLS_HEIGHT },
    
      { left: 731, top: 1, 
         width: 60, height: CREEPER_CELLS_HEIGHT },
    
      { left: 792, top: 1,  
         width: 62, height: CREEPER_CELLS_HEIGHT },
    
      { left: 855, top: 1,  
         width: 64, height: CREEPER_CELLS_HEIGHT },
       
      { left: 920, top: 1, 
         width: 66, height: CREEPER_CELLS_HEIGHT },
    
      { left: 988, top: 1,  
         width: 62, height: CREEPER_CELLS_HEIGHT }
   ];

   const CREEPER_CELLS_LEFT = [
      { left: 994, top: 69, 
        width: 59, height: CREEPER_CELLS_HEIGHT },

      { left: 941, top: 69, 
         width: 52, height: CREEPER_CELLS_HEIGHT },

      { left: 883, top: 69, 
         width: 58, height: CREEPER_CELLS_HEIGHT },

      { left: 821, top: 69, 
         width: 61, height: CREEPER_CELLS_HEIGHT },

      { left: 758, top: 69,  
         width: 61, height: CREEPER_CELLS_HEIGHT },

      { left: 678, top: 69,  
         width: 79, height: CREEPER_CELLS_HEIGHT },
   
      { left: 591, top: 69, 
         width: 86, height: CREEPER_CELLS_HEIGHT },

      { left: 497, top: 69,  
         width: 94, height: CREEPER_CELLS_HEIGHT },
         
      { left: 436, top: 69, 
         width: 60, height: CREEPER_CELLS_HEIGHT },
    
      { left: 382, top: 69, 
         width: 53, height: CREEPER_CELLS_HEIGHT },
    
      { left: 324, top: 69, 
         width: 57, height: CREEPER_CELLS_HEIGHT },
    
      { left: 263, top: 69, 
         width: 60, height: CREEPER_CELLS_HEIGHT },
    
      { left: 200, top: 69,  
         width: 62, height: CREEPER_CELLS_HEIGHT },
    
      { left: 135, top: 69,  
         width: 64, height: CREEPER_CELLS_HEIGHT },
       
      { left: 68, top: 69, 
         width: 66, height: CREEPER_CELLS_HEIGHT },
    
      { left: 4, top: 69,  
         width: 62, height: CREEPER_CELLS_HEIGHT }
   ];

   const JOKER_CELLS_RIGHT = [
      { left: 283, top: 55, 
        width: 47, height: JOKER_CELLS_HEIGHT },

      { left: 227, top: 55, 
         width: 47, height: JOKER_CELLS_HEIGHT },

      { left: 171, top: 55, 
         width: 47, height: JOKER_CELLS_HEIGHT },

      { left: 115, top: 55, 
         width: 47, height: JOKER_CELLS_HEIGHT },

      { left: 58, top: 55,  
         width: 48, height: JOKER_CELLS_HEIGHT },

      { left: 2, top: 55,  
         width: 48, height: JOKER_CELLS_HEIGHT }
   ];

   const JOKER_CELLS_LEFT = [
      { left: 2, top: 1, 
        width: 47, height: JOKER_CELLS_HEIGHT },

      { left: 58, top: 1, 
         width: 47, height: JOKER_CELLS_HEIGHT },

      { left: 115, top: 1, 
         width: 47, height: JOKER_CELLS_HEIGHT },

      { left: 171, top: 1, 
         width: 47, height: JOKER_CELLS_HEIGHT },

      { left: 227, top: 1,  
         width: 48, height: JOKER_CELLS_HEIGHT },

      { left: 283, top: 1,  
         width: 48, height: JOKER_CELLS_HEIGHT }
   ];

   const SCORPION_CELLS_RIGHT = [
      { left: 0, top: 3, 
        width: 38, height: SCORPION_CELLS_HEIGHT },

      { left: 39, top: 3, 
         width: 40, height: SCORPION_CELLS_HEIGHT },

      { left: 80, top: 3, 
         width: 40, height: SCORPION_CELLS_HEIGHT },

      { left: 121, top: 3, 
         width: 40, height: SCORPION_CELLS_HEIGHT },

      { left: 162, top: 3,  
         width: 41, height: SCORPION_CELLS_HEIGHT }
   ];

   const SCORPION_CELLS_LEFT = [
      { left: 165, top: 83, 
        width: 38, height: SCORPION_CELLS_HEIGHT },

      { left: 124, top: 83, 
         width: 40, height: SCORPION_CELLS_HEIGHT },

      { left: 83, top: 83, 
         width: 40, height: SCORPION_CELLS_HEIGHT },

      { left: 42, top: 83, 
         width: 40, height: SCORPION_CELLS_HEIGHT },

      { left: 0, top: 83,  
         width: 41, height: SCORPION_CELLS_HEIGHT }
   ];
   //#endregion