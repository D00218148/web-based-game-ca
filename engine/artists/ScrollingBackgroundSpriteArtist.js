
/**
 * Performs multiple draws (left, centre, right) of a background image that enable background scrolling
 * @author
 * @version 1.0
 * @class ScrollingBackgroundSpriteArtist
 */
class ScrollingBackgroundSpriteArtist 
{
    constructor(context, spritesheet, 
        sourcePosition, sourceDimensions, /*add params*/)
    {
        this.context = context;
        this.spritesheet = spritesheet;
        this.sourcePosition = sourcePosition;
        this.sourceDimensions = sourceDimensions;

        //to do...
    }

    Update(gameTime, parent)
    {
        //to do...
    }

    Draw(gameTime, parent)
    {
        //to do...
    }

    //to do...Equals()

    Clone()
    {
        //to do...
    }

    ToString()
    {
        //to do...   
    }
}