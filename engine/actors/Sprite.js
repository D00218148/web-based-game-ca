/**
 * Represents any drawn entity within a game with position information (e.g. player, enemy, menu button, HUD element)
 * @author
 * @version 1.0
 * @class Sprite
 */

class Sprite extends Actor2D {
    constructor(id, actorType, transform2D, artist, statusType) {
        super(id, actorType, transform2D, statusType);
        this.artist = artist;
    }


    /* #region  Properties */
    get Artist() {
        return this.artist;
    }
    set Artist(artist) {
        this.artist = artist;
    }
    /* #endregion */

    /* #region  Equals, Clone, ToString */
    Equals(other) {
        if (other == null || other == undefined || !other instanceof Sprite)
            throw 'Error: One or more objects is null, undefined, or not type ' + this.constructor.name;

        return this.id == other.ID && this.actorType == other.ActorType && this.transform2D.Equals(other.Transform2D) && this.artist.Equals(other.Artist);   
    }

    Clone() {
        //make a clone of the actor
        let clone = new Sprite("clone - " + this.id, this.actorType, this.transform2D.Clone(), this.artist.Clone(), this.statusType);

        //now clone all the actors attached behaviors
        for(let behavior of this.behaviors)
            clone.AttachBehavior(behavior.Clone());
        
        //lastly return the actor
        return clone;
    }

    ToString() {
        return "[" + this.id + "," + this.actorType + "," + this.transform2D.ToString() + "]";
    }
    /* #endregion */

    Update(gameTime)
    {
        //if we have an attached artist and we are supposed to update the sprite then update the artist 
        if (this.artist != null && 
            (this.StatusType & StatusType.IsUpdated) != 0)
        {
             this.artist.Update(gameTime, this);   
            //call Actor2D::Update() to update any attached behaviors
            super.Update(gameTime);     
        }
    }
    Draw(gameTime) {

        //if we have an attached artist and we are supposed to draw the sprite then draw 
        if (this.artist != null && (this.StatusType & StatusType.IsDrawn) != 0)
            this.artist.Draw(gameTime, this);
    }
}