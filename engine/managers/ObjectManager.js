/**
 * This is the base class for your own games implementation of ObjectManager e.g. MyObjectManager. An ObjectManager
 * is responsible for storing, updating, and drawing all the sprites within the game.
 * @author
 * @version 1.0
 * @class ObjectManager
 */

class ObjectManager{

    id = "";
    ctx;
    debugEnabled = false;

    constructor(id, ctx, debugEnabled){
        this.id = id;
        this.ctx = ctx;
        this.debugEnabled = debugEnabled;
    }

    get DebugEnabled() 
    {
        return this.debugEnabled;
    }

    Update(gameTime){

    }

    Draw(gameTime){

    }

    DrawDebugBoundingBox(color, boundingBox)
    {
        ctx.beginPath();
        ctx.strokeStyle = color;
        ctx.rect(boundingBox.X, boundingBox.Y, boundingBox.Width, boundingBox.Height);
        ctx.stroke();
    }
}